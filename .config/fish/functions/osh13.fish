# see https://medium.com/@reedrehg/run-an-odoo-repl-ipython-prompt-7f10d0d5d824
function osh13
         set -l options 'h/help' 'd/database='
         argparse -n upgrade  $options -- $argv
         or return

         if set -q _flag_help
           echo "-d/--database [DATABASE]"
           return 0
         end

         if not set -q _flag_database
             set_color red
             echo "Database is required"
             set_color normal
             return 1
         end
         docker-compose  run --rm --entrypoint=/opt/odoo/odoo13/odoo-bin odoo13_app shell --addons-path=/opt/odoo/enterprise_addons13,/opt/odoo/odoo13/addons,/opt/odoo/ls_addons13,/opt/odoo/external_addons13/oca/web,/opt/odoo/external_addons13/oca/social --workers=0 --xmlrpc-port=8888 --longpolling-port=8899 -d "$_flag_database" --db_host=odoo13_db --db_user=odoo --db_password=odoo

end

function complete_pg_database_abc
    # based on the version in /usr/share/fish/completions
    psql -XAtqwlF \t ^/dev/null | awk 'NF > 1 { print $1 }'
end

complete -c osh13 --no-files -s d -l database -r -a '(complete_pg_database_abc)'