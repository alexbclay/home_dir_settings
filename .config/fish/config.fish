set -x NVM_DIR "$HOME/.nvm"
set -x DENO_INSTALL "$HOME/.deno"
eval (dircolors /home/alexbclay/.dir_colors/dircolors | head -n 1 | sed 's/^LS_COLORS=/set -x LS_COLORS /;s/;$//')
set -Up fish_user_paths ~/.cargo/bin
# fish_add_path /usr/local/go/bin
# fish_add_path $HOME/go/bin
fish_add_path $DENO_INSTALL/bin
fish_add_path /home/alexbclay/.local/bin
pyenv init - | source